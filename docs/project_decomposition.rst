Project decomposition
======================

1. Cloud infrastructure setup
------------------------------
**Time:** 16.05.2022 - 01.06.2022

- Setup a Spark cluster with 3 data nodes in Yandex Cloud (YC) .
- Generate a 100GB sample of simulated data https://fraud-detection-handbook.github.io/fraud-detection-handbook/Chapter_3_GettingStarted/SimulatedDataset.html
- Upload all the generated data to the cluster in the Hadoop Distributed File System (HDFS).
- Estimate the monthly cost of maintaining the cluster. Suggest ways of cost optimization and implement them.

**Output:** prepared YC infrastructure

2. Data collection job scheduling 
----------------------------------
**Time:** 02.06.2022 - 18.06.2022

- Write a script that generates a new sample of data and saves it to HDFS.
- Automate a regular launch via AirFlow.

**Output:** data to be used for modeling

3. Data analysis
------------------
**Time:** 19.06.2022 - 22.06.2022

Manual process

**Output:** EDA notebooks

4. Development and experimentation
-----------------------------------
**Time:** 23.06.2022 - 06.08.2022

Iteratively try out new ML algorithms and new modeling where the experiment steps are orchestrated with Airflow. Model analysis - manual process.

4.1 Build scalable data preparation pipeline
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Time:** 23.06.2022 - 11.07.2022

- Automate data validation
- Write pyspark code for cleaning and features extracting from data.
- Save the results in HDFS.
- Create a virtual machine and run AirFlow on it.
- Automate a launch of a script to process new data via AirFlow on a schedule.

**Output:** data preparation pipeline

4.2 Automate model retraining
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Time:** 12.07.2022 - 03.08.2022

- Train the model with pyspark for the fraud detection task.
- Set up MLFlow to store model artifacts (Object storage as backend).
- Save artifacts with a model in MLFLow.
- Set up AirFlow to regularly retrain the model on new data

**Output:** model retraining pipeline

4.3 Automate model validation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
**Time:** 04.08.2022 - 06.08.2022

- Choose a strategy for model validation.
- Evaluate the model metrics for the selected strategy. (Model evaluation)
- Prepare a new model and conduct A/B testing for validation.
- Add a step for model validation and generating a report to AirFlow.

**Output:** model validation pipeline

**Block output:** the source code of the ML pipeline steps that are then pushed to a source repository.

5. Pipeline continuous integration
-----------------------------------
**Time:** 07.08.2022 - 10.08.2022

Build, test, and package the pipeline and its components when new code is committed or pushed to the source code repository.

Include the following tests:

- Unit testing features engineering logic.
- Unit testing the different methods implemented in the model.
- Testing that the model training converges.
- Testing that the model training doesn't produce NaN values due to dividing by zero or manipulating small or large values.
- Testing that each component in the pipeline produces the expected artifacts.
- Testing integration between pipeline components.

**Outputs:** pipeline components (packages, executables, and artifacts) to be deployed in a later stage.

6. Streaming Inference with Spark Streaming
--------------------------------------------
**Time:** 11.08.2022 - 24.08.2022

- Write a script that generates a new sample of data and saves it to Kafka.
- Create Spark job, which on the stream applies the model to data from kafka and saves the results to another topic.
- Test created Spark job.

**Output:** data to be used to test CD

7. Pipeline continuous delivery
-------------------------------
**Time:** 25.08.2022 - 31.08.2022

Deploy the artifacts produced by the CI stage to the target environment.

To do list:

- Verify the compatibility of the model with the target infrastructure before deploying the model. E.g., verify that the packages required by the model are installed in the serving environment, and that the memory, compute, and accelerator resources are available.
- Test the prediction service by calling the service API with the expected inputs, and ensure getting expected response.
- Test prediction service performance, which involves load testing the service to capture metrics such as queries per seconds (QPS) and model latency.
- Validate the data either for retraining or batch prediction.
- Verify that models meet the predictive performance targets before they are deployed.

- Make Kubernetes (k8s) deployment of a ML Rest API in the YC:
    - Build REST API for models using python
    - Implement in github CI/CD actions with tests, Docker containerization and its publishing in the registry.
    - Create k8s manifest to start the service.
    - Create k8s 3-nodes cluster in YC
    - Run the service in k8s and test it through the public API.

- Implement automated deployment to a test environment. E.g., a deployment that is triggered by pushing code to the development branch.
- Implement semi-automated deployment to a pre-production environment. E.g., a deployment that is triggered by merging code to the main branch after reviewers approve the changes.
- Implement manual deployment to a production environment after several successful runs of the pipeline on the pre-production environment.

**Output:** a deployed pipeline with the new implementation of the model.

8. Automated triggering
------------------------
**Time:** 01.09.2022 - 03.09.2022

The pipeline is automatically executed in production based on a schedule or in response to a trigger.

**Output:** a trained model that is pushed to the model registry.

9. Model continuous delivery
-----------------------------
**Time:** 04.09.2022 - 07.09.2022

Serve the trained model as a prediction service for the predictions.

**Output:** a deployed model prediction service.

10. Monitoring
---------------
**Time:** 08.09.2022 - 19.09.2022

Collect statistics on the model performance based on live data.

To do list:

- Write a script to generate data and inference through the API.
- Add model metrics generation for Prometheus to the web service
- Deploy Prometheus and Grafana on Kubernetes and add metrics collection
- Create and Manage Grafana Alerts
- Run the script to generate data and inference.
- Make sure metrics are collected and alerts are being processed.

**Output:** a trigger to execute the pipeline or to execute a new experiment cycle.