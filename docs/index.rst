Welcome to Credit Card Fraud Detection's documentation!
=======================================================

Credit card fraud detection is a challenging task: the profiles of normal 
and fraudulent behaviors change constantly, credit card fraud data sets are 
highly skewed. The performance of fraud detection in credit card transactions
is greatly affected by the sampling approach on the dataset, selection of 
variables and detection techniques used.

The goal of the current project is deploying an ML pipeline that can automate
the retraining and deployment of new models. A CI/CD system automatically tests
and deploys new pipeline implementations. This system should help to cope with
rapid changes in data and business environment.

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   project_metrics
   project_decomposition


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
