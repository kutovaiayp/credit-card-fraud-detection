Project metrics
================

The purpose of the credit card fraud detection system (FDS) is to provide
investigators with alerts, that is, a set of transactions that are assumed to
be the most suspicious. These transactions are manually checked, by contacting
the cardholder. The process of contacting cardholders is time-consuming, and 
the number of fraud investigators is limited. The number of alerts that may be
checked during a given period is therefore necessarily limited.

Fraud detection is most commonly addressed as a binary classification problem:
A fraud detection system receives transactions, and its goal is to predict whether
they are likely to be genuine, or fraudulent. A fraud detection system outputs a
fraud score, and classifies it as genuine or fraudulent. The decision to classify
a transaction as fraudulent can be made by setting a threshold when transactions 
with scores higher than this threshold are considered fraudulent.

To perform a more general comparison, it is better to use threshold-free metrics,
which aim at characterizing with a single number the performance of a classifier 
across all possible thresholds. Two well-known threshold-free assessment techniques
are the Receiving Operating Characteristic (ROC) curve and the Precision-Recall (PR)
curve. Both techniques allow providing an overall performance score, in the form of 
the area under the curve (AUC) measure.

Receiving Operating Characteristic (ROC) curve
-----------------------------------------------

Due to the highly imbalanced nature of the problem, ROC curve is not practical to use:
99.9% of what is represented on the ROC curve has little relevance from the perspective
of an operational fraud detection system where fraudulent transactions must be checked
by a limited team of investigators.

Precision-Recall (PR) curve
----------------------------
Another threshold-free metric, a PR curve, is useful to highlight the performances of 
FDS for low false positive rate values. However, PR curves remain difficult to interpret
from an operational point of view.

Card Precision Top-k
---------------------
From a more operational perspective, the relevant credit card FDS should explicitly
consider their benefits for fraud investigators. Precision top-k metrics aim at quantifying
the performance of a FRS in this setting, computing the precision for the top k ranked
transactions. Precisions are computed daily, reflecting the precisions obtained for a
working day of fraud investigators. The parameter quantifies the maximum number of alerts 
that can be checked by investigators in a day.

Multiple fraudulent transactions from the same card should count as a single correct 
detection since investigators check all the recent transactions when contacting cardholders.
The resulting metric is the Card Precision Top-k, or CP@k, and quantifies the number of 
correctly detected compromised cards out of the k cards which have the highest risks of frauds.

CP@k can be computed by slightly amending the implementation of the Precision top-k given 
above. More specifically, instead of simply sorting transactions by decreasing order of 
their fraud probabilities, we first group transactions by customer ID. For each customer ID,
we then take the maximum value of the fraud probability and the fraud label. The card precision
top-k is finally computed by sorting customer IDs by decreasing order of the fraud probabilities
and computing the precision for the set of cards with the highest fraud probabilities.