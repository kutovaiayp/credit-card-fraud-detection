curl -LfO 'https://airflow.apache.org/docs/apache-airflow/2.3.2/docker-compose.yaml' > docker-compose.yaml
mkdir -p ./dags ./logs ./plugins ./dags/scripts ./dags/tmp ./generated_files
sudo chmod u=rwx,g=rwx,o=rwx /home/ubuntu/generated_files

git clone https://gitlab.com/kutovaiayp/credit-card-fraud-detection.git
cp credit-card-fraud-detection/dags/generate-data-dag.py.py dags/generate-data-dag.py
cp credit-card-fraud-detection/dags/scripts/* dags/scripts/
cp credit-card-fraud-detection/docker-compose.yaml .
chmod +x dags/scripts/test.sh

echo -e "AIRFLOW_UID=$(id -u)" > .env
AIRFLOW_UID=50000

docker-compose up airflow-init
docker-compose up

docker ps

docker-compose down
docker-compose down --volumes --rmi all

ENDPOINT_URL="http://localhost:8080/"
curl -X GET  --user "airflow:airflow" "${ENDPOINT_URL}/api/v1/pools"
