pip install dvc[all]
dvc init
git commit -m "Initialize DVC"
dvc remote add -d s3store s3://generated-data/01_raw
dvc remote modify s3store endpointurl https://storage.yandexcloud.net

git rm -r --cached 'data'
git commit -m "stop tracking data"
git push
dvc add data 
git add data.dvc
dvc config core.autostage true
dvc install

dvc add data # after any chages in 'data' folder before commit
dvc pull # after git pull to get changes from remote

dvc remove data.dvc
