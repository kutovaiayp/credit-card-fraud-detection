ssh -L 8080:localhost:8080 ubuntu@51.250...

curl -sSL install.astronomer.io | sudo bash -s
astro version

mkdir iris-project
cd iris-project

astro dev init
astro dev start


export PATH="/home/ubuntu/.local/bin:$PATH"
export PATH="/home/astro/.local/bin:$PATH"
source ~/.bashrc

sudo apt install python3-pip
python3 -m pip install --upgrade pip


pip install kedro
kedro info

mkdir kedro-airflow-iris
cd kedro-airflow-iris
astro dev init

kedro new --starter=pandas-iris
cp -r new-kedro-project/* .
cp -r C:/Users/ASER/credit_card_fraud_detection/* . 

rm -r new-kedro-project
pip install kedro-airflow~=0.4
pip install -r src/requirements.txt

mkdir conf/airflow
vim conf/airflow/catalog.yml
kedro package
echo "src/" >> .dockerignore
vim Dockerfile

kedro airflow create --target-dir=dags/ --env=airflow
astro dev start
