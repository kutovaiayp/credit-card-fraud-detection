import subprocess

def run_cmd(args_list):
    """
    run linux commands
    """
    # import subprocess
    print('Running system command: {0}'.format(' '.join(args_list)))
    proc = subprocess.Popen(args_list, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    s_output, s_err = proc.communicate()
    s_return =  proc.returncode
    return s_return, s_output, s_err

#Install pandarallel
(ret, out, err)= run_cmd(['python3', '-m', 'pip', 'install', 'pandarallel'])

#Generate file in Python
(ret, out, err)= run_cmd(['cp', 'credit-card-fraud-detection/data/01_raw/data_generator.py', '~/data_generator.py'])    
     
from data_generator import Data_generator
    
data_generator = Data_generator(
    n_customers=5, n_terminals=10, radius=50, start_date='2021-07-01', nb_days=10, random_state=20
    )
data_generator.get_data()

#Run Hadoop mkdir command in Python
(ret, out, err)= run_cmd(['hadoop', 'fs', '-mkdir', '-p', 'simulated-data-raw'])

#Run Hadoop put command in Python
(ret, out, err)= run_cmd(['hadoop', 'fs', '-put',
'simulated-data-raw/2021-07-01_10_days.pkl', 'simulated-data-raw/2021-07-01_10_days.pkl'])

#Run Hadoop ls command in Python
(ret, out, err)= run_cmd(['hadoop', 'fs', '-ls', 'simulated-data-raw'])
print(out.decode("latin"))