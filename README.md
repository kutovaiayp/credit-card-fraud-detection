# Welcome to Credit Card Fraud Detection’s documentation!

Credit card fraud detection is a challenging task: the profiles of normal and fraudulent behaviors change constantly, credit card fraud data sets are highly skewed. The performance of fraud detection in credit card transactions is greatly affected by the sampling approach on the dataset, selection of variables and detection techniques used.

The goal of the current project is deploying an ML pipeline that can automate the retraining and deployment of new models. A CI/CD system automatically tests and deploys new pipeline implementations. This system should help to cope with rapid changes in data and business environment.


## Contents:

* [Project metrics](https://gitlab.com/kutovaiayp/credit-card-fraud-detection/-/blob/main/docs/project_metrics.rst)   
* [Project decomposition](https://gitlab.com/kutovaiayp/credit-card-fraud-detection/-/blob/main/docs/project_decomposition.rst)
* [Work plan](https://gitlab.com/kutovaiayp/credit-card-fraud-detection/-/issues)

  


©2022, Yanina Kutovaya. | Powered by [Kedro v0.18.0](https://kedro.readthedocs.io/en/stable/) data science project template.