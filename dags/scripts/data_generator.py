import numpy as np
import pandas as pd
from pandarallel import pandarallel
import random
import time
import datetime
import os
import shutil
import matplotlib.pyplot as plt
import seaborn as sns



class Data_generator():
    def __init__(self, n_customers, n_terminals, radius, 
                 start_date, nb_days, random_state):
        
        self.n_customers = n_customers
        self.n_terminals = n_terminals
        self.radius = radius
        self.start_date = start_date
        self.nb_days = nb_days
        self.random_state =random_state
        
        self.customer_profiles_table = None
        self.terminal_profiles_table = None
        self.x_y_terminals = None
        self.transactions_df = None
              
        self.tx_stats = None
                 
                 
    def __repr__(self):
        return '\n'.join([
            f'The number of customers: {self.n_customers}',
            f'The number of terminals: {self.n_terminals}',
            f'Radius:\t\t\t {self.radius}',
            f'Start date:\t\t {self.start_date}',
            f'The number of days:\t {self.nb_days}'])



def generate_customer_profiles_table(self):   
    np.random.seed(self.random_state)        
    customer_id_properties=[]
    
    # Generate customer properties from random distributions 
    for customer_id in range(self.n_customers):        
        x_customer_id = np.random.uniform(0, 100)
        y_customer_id = np.random.uniform(0, 100)
        
        mean_amount = np.random.uniform(5, 100)      # Arbitrary (but sensible) value 
        std_amount = mean_amount / 2                 # Arbitrary (but sensible) value
        
        mean_nb_tx_per_day = np.random.uniform(0, 4) # Arbitrary (but sensible) value 
        
        customer_id_properties.append(
            [customer_id, x_customer_id, y_customer_id, mean_amount, std_amount, mean_nb_tx_per_day])
    
    cols = ['CUSTOMER_ID', 'x_customer_id', 'y_customer_id', 'mean_amount', 'std_amount', 'mean_nb_tx_per_day']    
    self.customer_profiles_table = pd.DataFrame(customer_id_properties, columns=cols) 

   
Data_generator.generate_customer_profiles_table = generate_customer_profiles_table 



def generate_terminal_profiles_table(self):    
    np.random.seed(self.random_state)        
    terminal_id_properties=[]
    
    # Generate terminal properties from random distributions 
    for terminal_id in range(self.n_terminals):        
        x_terminal_id = np.random.uniform(0, 100)
        y_terminal_id = np.random.uniform(0, 100)
        
        terminal_id_properties.append([terminal_id, x_terminal_id, y_terminal_id])
    
    cols = ['TERMINAL_ID', 'x_terminal_id', 'y_terminal_id']
    self.terminal_profiles_table = pd.DataFrame(terminal_id_properties, columns=cols)
    self.x_y_terminals = self.terminal_profiles_table[['x_terminal_id','y_terminal_id']].to_numpy()

    
Data_generator.generate_terminal_profiles_table = generate_terminal_profiles_table 



def get_list_terminals_within_radius(self, customer_profile): 
    # Use numpy arrays in the following to speed up computations    
    
    # Location (x,y) of customer as numpy array
    x_y_customer = customer_profile[['x_customer_id','y_customer_id']].to_numpy()
    
    # Squared difference in coordinates between customer and terminal locations
    squared_diff_x_y = np.square(x_y_customer - self.x_y_terminals)
    
    # Sum along rows and compute suared root to get distance
    dist_x_y = np.sqrt(np.sum(squared_diff_x_y, axis=1))
    
    # Get the indices of terminals which are at a distance less than r
    available_terminals = list(np.where(dist_x_y < self.radius)[0])
    
    # Return the list of terminal IDs
    return available_terminals


def associate_terminals_to_customers(self):
    pandarallel.initialize(progress_bar=True)    
    df_grouped = self.customer_profiles_table.groupby('CUSTOMER_ID')
    self.customer_profiles_table['available_terminals'] = df_grouped.parallel_apply(self.get_list_terminals_within_radius)  
    

Data_generator.get_list_terminals_within_radius = get_list_terminals_within_radius
Data_generator.associate_terminals_to_customers = associate_terminals_to_customers



def generate_transactions_table(self, customer_profile):    
    customer_transactions = []
    
    customer_profile = customer_profile.to_numpy()[0]
    
    customer_id = int(customer_profile[0])
    mean_nb_tx_per_day = customer_profile[5]
    mean_amount = customer_profile[3]
    std_amount = customer_profile[4]
    available_terminals = customer_profile[6]    
    
    random.seed(customer_id)
    np.random.seed(customer_id)
    
    # For all days
    for day in range(self.nb_days):        
        # Random number of transactions for that day 
        nb_tx = np.random.poisson(mean_nb_tx_per_day)
        
        # If nb_tx positive, let us generate transactions
        if nb_tx > 0:            
            for tx in range(nb_tx):                
                # Time of transaction: Around noon, std 20000 seconds. This choice aims at simulating the fact that 
                # most transactions occur during the day.
                time_tx = int(np.random.normal(86400/2, 20000))
                
                # If transaction time between 0 and 86400, let us keep it, otherwise, let us discard it
                if (time_tx > 0) and (time_tx < 86400):
                    
                    # Amount is drawn from a normal distribution  
                    amount = np.random.normal(mean_amount, std_amount)
                    
                    # If amount negative, draw from a uniform distribution
                    if amount < 0:
                        amount = np.random.uniform(0, mean_amount * 2)
                    
                    amount = np.round(amount, decimals=2)
                    
                    if len(available_terminals) > 0:                        
                        terminal_id = random.choice(available_terminals)
                    
                        customer_transactions.append(
                            [time_tx + day * 86400, day, customer_id, terminal_id, amount])
            
    cols = ['TX_TIME_SECONDS', 'TX_TIME_DAYS', 'CUSTOMER_ID', 'TERMINAL_ID', 'TX_AMOUNT']
    customer_transactions = pd.DataFrame(customer_transactions, columns=cols)
    
    if len(customer_transactions) > 0:
        customer_transactions['TX_DATETIME'] = pd.to_datetime(customer_transactions['TX_TIME_SECONDS'], unit='s', origin=self.start_date)
        customer_transactions = customer_transactions[['TX_DATETIME','CUSTOMER_ID', 'TERMINAL_ID', 'TX_AMOUNT','TX_TIME_SECONDS', 'TX_TIME_DAYS']]
    
    return customer_transactions 


Data_generator.generate_transactions_table = generate_transactions_table



def get_all_customers_transactions(self): 
    pandarallel.initialize(progress_bar=True) 
    df_grouped = self.customer_profiles_table.groupby('CUSTOMER_ID')  
    res_list = df_grouped.parallel_apply(self.generate_transactions_table)
    res_list.index = range(len(res_list))
    transactions_df = res_list 
    
    # Sort transactions chronologically
    transactions_df = transactions_df.sort_values('TX_DATETIME')
    
    # Reset indices, starting from 0
    transactions_df.reset_index(inplace=True, drop=True)
    transactions_df.reset_index(inplace=True)
    
    # TRANSACTION_ID are the dataframe indices, starting from 0
    transactions_df.rename(columns = {'index':'TRANSACTION_ID'}, inplace = True)
    
    self.transactions_df = transactions_df    

    
Data_generator.get_all_customers_transactions = get_all_customers_transactions



def plot_data_distributions(self, N=10000):
    distribution_amount_times_fig, ax = plt.subplots(1, 2, figsize=(18,4))

    amount_val = self.transactions_df[self.transactions_df['TX_TIME_DAYS'] < 10]['TX_AMOUNT'].sample(n=N).values
    time_val = self.transactions_df[self.transactions_df['TX_TIME_DAYS'] < 10]['TX_TIME_SECONDS'].sample(n=N).values

    sns.distplot(amount_val, ax=ax[0], color='r', hist = True, kde = False)
    ax[0].set_title('Distribution of transaction amounts', fontsize=14)
    ax[0].set_xlim([min(amount_val), max(amount_val)])
    ax[0].set(xlabel = "Amount", ylabel="Number of transactions")

    # We divide the time variables by 86400 to transform seconds to days in the plot
    sns.distplot(time_val/86400, ax=ax[1], color='b', bins = 100, hist = True, kde = False)
    ax[1].set_title('Distribution of transaction times', fontsize=14)
    ax[1].set_xlim([min(time_val/86400), max(time_val/86400)])
    ax[1].set_xticks(range(10))
    ax[1].set(xlabel = "Time (days)", ylabel="Number of transactions");


Data_generator.plot_data_distributions = plot_data_distributions



def add_frauds(self):    
    # By default, all transactions are genuine
    self.transactions_df['TX_FRAUD'] = 0
    self.transactions_df['TX_FRAUD_SCENARIO'] = 0
    
    # Scenario 1
    self.transactions_df.loc[self.transactions_df['TX_AMOUNT'] > 220, 'TX_FRAUD'] = 1
    self.transactions_df.loc[self.transactions_df['TX_AMOUNT'] > 220, 'TX_FRAUD_SCENARIO'] = 1
    nb_frauds_scenario_1 = self.transactions_df['TX_FRAUD'].sum()
    print(f'Number of frauds from scenario 1: {nb_frauds_scenario_1}')
    
    # Scenario 2
    for day in range(self.transactions_df['TX_TIME_DAYS'].max()):        
        compromised_terminals = self.terminal_profiles_table['TERMINAL_ID'].sample(n=2, random_state=day)
        
        compromised_transactions = self.transactions_df[(self.transactions_df['TX_TIME_DAYS'] >= day) & 
                                                        (self.transactions_df['TX_TIME_DAYS'] < day + 28) & 
                                                        (self.transactions_df['TERMINAL_ID'].isin(compromised_terminals))]
                            
        self.transactions_df.loc[compromised_transactions.index,'TX_FRAUD'] = 1
        self.transactions_df.loc[compromised_transactions.index,'TX_FRAUD_SCENARIO'] = 2
    
    nb_frauds_scenario_2 = self.transactions_df['TX_FRAUD'].sum() - nb_frauds_scenario_1
    print(f'Number of frauds from scenario 2: {nb_frauds_scenario_2}')
    
    # Scenario 3
    for day in range(self.transactions_df['TX_TIME_DAYS'].max()):        
        compromised_customers = self.customer_profiles_table['CUSTOMER_ID'].sample(n=3, random_state=day).values
        
        compromised_transactions = self.transactions_df[(self.transactions_df['TX_TIME_DAYS'] >= day) & 
                                                        (self.transactions_df['TX_TIME_DAYS'] < day + 14) & 
                                                        (self.transactions_df['CUSTOMER_ID'].isin(compromised_customers))]
        
        nb_compromised_transactions = len(compromised_transactions)        
        
        random.seed(day)
        index_frauds = random.sample(list(compromised_transactions.index.values), k = int(nb_compromised_transactions / 3))
        
        self.transactions_df.loc[index_frauds,'TX_AMOUNT'] = self.transactions_df.loc[index_frauds,'TX_AMOUNT'] * 5
        self.transactions_df.loc[index_frauds,'TX_FRAUD'] = 1
        self.transactions_df.loc[index_frauds,'TX_FRAUD_SCENARIO'] = 3
        
                             
    nb_frauds_scenario_3 = self.transactions_df['TX_FRAUD'].sum() - nb_frauds_scenario_2 - nb_frauds_scenario_1
    print(f'Number of frauds from scenario 3: {nb_frauds_scenario_3}')


Data_generator.add_frauds = add_frauds



def get_stats(self):
    #Number of transactions per day
    nb_tx_per_day = self.transactions_df.groupby(['TX_TIME_DAYS'])['CUSTOMER_ID'].count()
    
    #Number of fraudulent transactions per day
    nb_fraud_per_day = self.transactions_df.groupby(['TX_TIME_DAYS'])['TX_FRAUD'].sum()
    
    #Number of fraudulent cards per day
    nb_fraudcard_per_day = self.transactions_df[self.transactions_df['TX_FRAUD'] > 0].groupby(['TX_TIME_DAYS'])['CUSTOMER_ID'].nunique()
    
      
    n_days = len(nb_tx_per_day)
    tx_stats = pd.DataFrame({'value': pd.concat([nb_tx_per_day, nb_fraud_per_day, nb_fraudcard_per_day])}) 
    tx_stats['stat_type'] = ['nb_tx_per_day'] * n_days + ['nb_fraud_per_day'] *  n_days + ['nb_fraudcard_per_day'] * len(nb_fraudcard_per_day)
    
    self.tx_stats = tx_stats.reset_index() 
    
    
Data_generator.get_stats = get_stats



def plot_stats(self):
    sns.set(style='darkgrid')
    sns.set(font_scale=1.4)

    fraud_and_transactions_stats_fig = plt.gcf()

    fraud_and_transactions_stats_fig.set_size_inches(15, 8)
    stats = self.tx_stats 
    stats.loc[stats['stat_type'] == 'nb_tx_per_day', 'value'] = stats.loc[stats['stat_type'] == 'nb_tx_per_day', 'value'] / 50

    sns_plot = sns.lineplot(x="TX_TIME_DAYS", y="value", data=stats, hue="stat_type",
                            hue_order=["nb_tx_per_day","nb_fraud_per_day","nb_fraudcard_per_day"], legend=False)

    title = 'Total transactions, and number of fraudulent transactions \n and number of compromised cards per day'
    sns_plot.set_title(title, fontsize=20)
    sns_plot.set(xlabel = "Number of days since beginning of data generation", ylabel="Number")

    sns_plot.set_ylim([0, 300])

    labels_legend = ["# transactions per day (/50)", "# fraudulent txs per day", "# fraudulent cards per day"]

    sns_plot.legend(loc='upper left', labels=labels_legend,bbox_to_anchor=(1.05, 1), fontsize=15);
    
    
Data_generator.plot_stats = plot_stats



def save_data(self, DIR_OUTPUT):
    if not os.path.exists(DIR_OUTPUT):
        os.makedirs(DIR_OUTPUT)

    start_date = datetime.datetime.strptime(self.start_date, "%Y-%m-%d")

    for day in range(self.transactions_df['TX_TIME_DAYS'].max() + 1):
        transactions_day = self.transactions_df[self.transactions_df['TX_TIME_DAYS'] == day].sort_values('TX_TIME_SECONDS')

        date = start_date + datetime.timedelta(days=day)
        filename_output = date.strftime("%Y-%m-%d")+'.pkl'    

        transactions_day.to_pickle(DIR_OUTPUT + filename_output)
        
        
Data_generator.save_data = save_data



def save_data_one_piece(self, DIR_OUTPUT):
    if not os.path.exists(DIR_OUTPUT):
        os.makedirs(DIR_OUTPUT)
    
    self.transactions_df.sort_values('TX_TIME_SECONDS', inplace=True)

    start_date = datetime.datetime.strptime(self.start_date, "%Y-%m-%d")
    filename_output = start_date.strftime("%Y-%m-%d") + '_' + str(self.nb_days) + '_days' +'.pkl'
    self.transactions_df.to_pickle(DIR_OUTPUT + filename_output)
        
        
Data_generator.save_data_one_piece = save_data_one_piece



def get_data(self, DIR_OUTPUT='./simulated-data-raw/', one_piece=True):
    start_time=time.time()
    self.generate_customer_profiles_table()
    print("Time to generate customer profiles table: {0:.2}s".format(time.time()-start_time))
    
    start_time=time.time()
    self.generate_terminal_profiles_table()
    print("Time to generate terminal profiles table: {0:.2}s".format(time.time()-start_time))
    
    start_time=time.time()
    self.associate_terminals_to_customers()
    print("Time to associate terminals to customers: {0:.2}s".format(time.time()-start_time))
    
    start_time=time.time()
    self.get_all_customers_transactions()
    print('Time to generate transactions: {0:.2}s\n'.format(time.time() - start_time))
    
    start_time=time.time()
    self.add_frauds()
    print('Time to add frauds to transactions: {0:.2}s\n'.format(time.time() - start_time))
    
    self.get_stats()

    if one_piece:
        self.save_data_one_piece(DIR_OUTPUT)
    else:
        self.save_data(DIR_OUTPUT)
    
    data_size = os.path.getsize(DIR_OUTPUT) * 9.31e-10
    print(f'Generated data size = {data_size} GB')
    
    mean_fraud = self.transactions_df['TX_FRAUD'].mean() * 100
    print(f'Mean fraud = {mean_fraud} %')
    
Data_generator.get_data = get_data  



if __name__ == "__main__":
    
    data_generator = Data_generator(n_customers=5, n_terminals=10, radius=50,
                                start_date='2022-06-21', nb_days=1, random_state=20)
    #data_generator.get_data()
    data_generator.generate_customer_profiles_table()
    data_generator.generate_terminal_profiles_table()
    data_generator.associate_terminals_to_customers()
    data_generator.get_all_customers_transactions()
    data_generator.add_frauds()
    data_generator.transactions_df.sort_values('TX_TIME_SECONDS', inplace=True) 
    
    DIR_OUTPUT='/opt/airflow/dags/tmp/generated_data.pkl'   
    data_generator.transactions_df.to_pickle(DIR_OUTPUT) 