from airflow import DAG
from airflow.operators.bash import BashOperator
from datetime import datetime, timedelta


with DAG(dag_id="generate_data_dag",
        start_date=datetime(2022, 6, 21),
        schedule_interval='@daily',
        catchup=False
        ) as dag:

    generate_file = BashOperator(
        task_id='generate_file',
        bash_command='python3 /opt/airflow/dags/scripts/data_generator.py'
        )
    copy_to_hdfs = BashOperator(
        task_id='copy_to_hdfs',
        bash_command='curl -i -L  -X PUT -T /opt/airflow/dags/tmp/generated_data.pkl "http://rc1a-dataproc-m-f0sojvd9c666k3ht.mdb.yandexcloud.net:9870/webhdfs/v1/user/ubuntu/simulated-data-raw/generated_data.pkl?op=CREATE" '
        )
    generate_file >> copy_to_hdfs
